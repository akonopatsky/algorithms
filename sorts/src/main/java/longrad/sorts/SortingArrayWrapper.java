package longrad.sorts;

import longrad.sorts.utils.ArraySortUtil;

import java.util.Comparator;

public class SortingArrayWrapper<T> {
    private final T[] array;
    private final Comparator<T> comparator;

    public SortingArrayWrapper(T[] array, Comparator<T> comparator) {
        this.array = array;
        this.comparator = comparator;
    }

    public void exchange(int first, int second) {
        ArraySortUtil.exchange(array, first, second);
    }

    public int indexOfMin(int first, int last) {
        return ArraySortUtil.indexOfMin(array, first, last, comparator);
    }

    public boolean isLess(int firstIndex, int secondIndex) {
        return ArraySortUtil.isLess(array, firstIndex, secondIndex, comparator);
    }

    public T[] getArray() {
        return array;
    }

    public T get(int i) {
        return array[i];
    }
}
