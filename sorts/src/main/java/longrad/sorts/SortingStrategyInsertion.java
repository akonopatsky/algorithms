package longrad.sorts;

import java.util.Comparator;

public class SortingStrategyInsertion<T> implements SortingStrategy<T> {
    @Override
    public void sort(T[] array, Comparator<T> comparator) {
        SortingArrayWrapper<T> sortingObject = new SortingArrayWrapper<>(array, comparator);
        int n = array.length;
        for (int i = 1; i < n; i++) {
            for (int j = i; j > 0; j--) {
                if (sortingObject.isLess(j, j - 1)) {
                    sortingObject.exchange(j, j-1);
                }
                else {
                    break;
                }
            }
        }
    }
}
