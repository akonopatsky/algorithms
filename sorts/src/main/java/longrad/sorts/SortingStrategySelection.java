package longrad.sorts;

import java.util.Comparator;

public class SortingStrategySelection<T> implements SortingStrategy<T> {
    @Override
    public void sort(T[] array, Comparator<T> comparator) {
        SortingArrayWrapper<T> sortingObject = new SortingArrayWrapper<>(array, comparator);
        int n = array.length;
        for (int i = 0; i < n; i++) {
            sortingObject.exchange(i, sortingObject.indexOfMin(i, n));
        }
    }
}
