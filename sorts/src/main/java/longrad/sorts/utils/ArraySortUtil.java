package longrad.sorts.utils;

import java.util.Comparator;
import java.util.concurrent.ThreadLocalRandom;

public class ArraySortUtil {
    public static <T> void shuffle(T[] array) {
        for (int i = 0; i < array.length -1; i++) {
            int randomIndex = ThreadLocalRandom.current().nextInt(i + 1, array.length);
            exchange(array, i, randomIndex);
        }
    }

    public static <T> void exchange(T[] array, int i, int j) {
        T swap = array[i];
        array[i] = array[j];
        array[j] = swap;
    }

    public static <T> int indexOfMin(T[] array, int first, int last, Comparator<T> comparator) {
        int indexOfMin = first;
        for (int i = first + 1; i < last; i++) {
            if (isLess(array, i, indexOfMin, comparator)) {
                indexOfMin = i;
            }
        }
        return indexOfMin;
    }

    public static <T> boolean isLess(T[] array, int firstIndex, int secondIndex, Comparator<T> comparator) {
        return comparator.compare(array[firstIndex], array[secondIndex]) < 0;
    }

}
