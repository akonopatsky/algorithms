package longrad.sorts;

import longrad.sorts.utils.ArraySortUtil;

import java.util.Arrays;
import java.util.Comparator;

public class SortingStrategyMerge<T> implements SortingStrategy<T> {
    private Comparator<T> comparator;

    @Override
    public void sort(T[] array, Comparator<T> comparator) {
        int lastIndex = array.length - 1;
        T[] mergeArray = Arrays.copyOf(array, array.length);
        this.comparator = comparator;
        sort(array, mergeArray, 0, lastIndex);
        System.arraycopy(mergeArray, 0, array, 0, mergeArray.length);
    }

    private void sort(T[] array, T[] mergeArray, int first, int last) {
        int n = last - first;
        if (last <= first) {
            return;
        }
        int mid = first + n / 2;
        sort(mergeArray, array, first, mid);
        sort(mergeArray, array, mid + 1, last);
        merge(array, mergeArray, first, (mid), last);
    }

    private void merge(T[] array, T[] mergeArray, int first, int mid, int last) {
        int left = first;
        int right = mid+1;
        for (int i = first; i <= last; i++) {
            if (left > mid) {
                mergeArray[i] = array[right++];
            } else if (right > last) {
                mergeArray[i] = array[left++];
            } else if (ArraySortUtil.isLess(array, left, right, comparator)) {
                mergeArray[i] = array[left++];
            } else {
                mergeArray[i] = array[right++];
            }
        }
    }
}
