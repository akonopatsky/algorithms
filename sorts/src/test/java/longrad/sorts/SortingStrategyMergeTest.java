package longrad.sorts;

import longrad.sorts.helpers.SortTesting;

class SortingStrategyMergeTest extends SortTesting {

    public SortingStrategyMergeTest() {
        super(new SortingStrategyMerge<>(), 2001);
    }
}