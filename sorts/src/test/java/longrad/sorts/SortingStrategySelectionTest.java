package longrad.sorts;

import longrad.sorts.helpers.SortTesting;

import static org.junit.jupiter.api.Assertions.*;

class SortingStrategySelectionTest extends SortTesting {
    public SortingStrategySelectionTest() {
        super(new SortingStrategySelection<>(), 2000);
    }
}