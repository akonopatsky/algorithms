package longrad.sorts.helpers;

import longrad.sorts.utils.ArraySortUtil;

import java.util.Arrays;
import java.util.Comparator;

public class GeneratorTestData {
    public static String[] getStringShuffledSec(int sequenceSize) {
        String[] result = GeneratorTestData.getStringIncreasingSeq(sequenceSize);
        ArraySortUtil.shuffle(result);
        return result;
    }

    public static String[] getStringReverseSec(int sequenceSize) {
        String[] result = new String[sequenceSize];
        for (int i = 0; i < sequenceSize; i++) {
            result[i] = String.valueOf(sequenceSize - i);
        }
        return result;
    }

    public static String[] getStringIncreasingSeq(int sequenceSize) {
        String[] result = new String[sequenceSize];
        for (int i = 0; i < sequenceSize; i++) {
            result[i] = String.valueOf(i);
        }
        return result;
    }
}
