package longrad.sorts.helpers;

import longrad.sorts.SortingStrategy;
import longrad.sorts.utils.ArraySortUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class SortTesting {
    private final int sequenceSize;
    private final SortingStrategy<String> sortingStrategy;
    private String[] reverseSequence;
    private String[] increasingSequence;
    private String[] shuffledSequence;

    public SortTesting(SortingStrategy<String> sortingStrategy, int sequenceSize) {
        this.sequenceSize = sequenceSize;
        this.sortingStrategy = sortingStrategy;
    }

    @BeforeEach
    void setUp() {
        reverseSequence = GeneratorTestData.getStringReverseSec(sequenceSize);
        increasingSequence = GeneratorTestData.getStringIncreasingSeq(sequenceSize);
        shuffledSequence = GeneratorTestData.getStringShuffledSec(sequenceSize);
    }

    @Test
    @DisplayName("сортировка Comparator.naturalOrder() ")
    void sortNatural() {
        assertTrue(test(reverseSequence, sortingStrategy, Comparator.naturalOrder()));
        assertTrue(test(increasingSequence, sortingStrategy, Comparator.naturalOrder()));
        assertTrue(test(shuffledSequence, sortingStrategy, Comparator.naturalOrder()));
    }

    @Test
    @DisplayName("сортировка Comparator.reverseOrder() ")
    void sortReverse() {
        assertTrue(test(reverseSequence, sortingStrategy, Comparator.reverseOrder()));
        assertTrue(test(increasingSequence, sortingStrategy, Comparator.reverseOrder()));
        assertTrue(test(shuffledSequence, sortingStrategy, Comparator.reverseOrder()));
    }

    @Test
    @DisplayName("сортировка Comparator.comparing(String::length) ")
    void sortByLength() {
        assertTrue(test(reverseSequence, sortingStrategy, Comparator.comparing(String::length)));
        assertTrue(test(increasingSequence, sortingStrategy, Comparator.comparing(String::length)));
        assertTrue(test(shuffledSequence, sortingStrategy, Comparator.comparing(String::length)));
    }
    
    public static <T> boolean test(T[] array, SortingStrategy<T> sortingStrategy, Comparator<T> comparator) {
        sortingStrategy.sort(array, comparator);
        for (int i = 0; i < array.length-1; i++) {
            if (ArraySortUtil.isLess(array, i+1, i, comparator)) {
                return false;
            }
        }
        return true;
    }

}
