package longrad.sorts;

import longrad.sorts.helpers.SortTesting;

class SortingStrategyInsertionTest extends SortTesting{
    public SortingStrategyInsertionTest() {
        super(new SortingStrategyInsertion<>(), 2000);
    }
}